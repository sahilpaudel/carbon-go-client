package cache

import (
	"fmt"
	"github.com/allegro/bigcache/v3"
	"sync"
	"time"
)

var cache *bigcache.BigCache
var once sync.Once

func GetInstance() *bigcache.BigCache {
	once.Do(func() {
		fmt.Println("Cache initial")
		cache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(1 * time.Minute))
	})

	return cache
}