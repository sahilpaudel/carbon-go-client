package carbon_go_client

import (
	"bitbucket.org/sahilpaudel/carbon-go-client/cache"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
)

func Init(ruleSetId string) {
	localCache := cache.GetInstance()
	baseURL := os.Getenv("CARBON_BASE_URL")
	authToken := os.Getenv("CARBON_AUTH_TOKEN")
	ruleSetEndPoint := fmt.Sprintf("%s/rules_engine/api/rule_sets/%s", baseURL, ruleSetId)
	ruleSetVersionsEndPoint := fmt.Sprintf("%s/rules_engine/api/rule_set_versions?rule_set_id=%s&is_archived=false&page=1", baseURL, ruleSetId)

	ruleSetURL, _ := url.Parse(ruleSetEndPoint)
	ruleSetVersionsURL, _ := url.Parse(ruleSetVersionsEndPoint)

	req := &http.Request{
		Method: http.MethodGet,
		URL: ruleSetURL,
		Header: map[string][]string{
			"Authorization": {
				authToken,
			},
		},
	}

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		log.Fatal(err)
	}

	data, err := ioutil.ReadAll(res.Body)

	_ = localCache.Set("RuleSet", data)


	req2 := &http.Request{
		Method: http.MethodGet,
		URL: ruleSetVersionsURL,
		Header: map[string][]string{
			"Authorization": {
				authToken,
			},
		},
	}

	res2, err := http.DefaultClient.Do(req2)

	if err != nil {
		log.Fatal(err)
	}

	data2, err := ioutil.ReadAll(res2.Body)

	_ = localCache.Set("RuleSetVersions", data2)
}

