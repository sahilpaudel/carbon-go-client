package carbon_go_client

import (
	"bitbucket.org/sahilpaudel/carbon-go-client/cache"
	"github.com/robertkrimen/otto"
	"log"
	filepath2 "path/filepath"
)


func Evaluate(ruleSetName string, facts string, versionId string) interface{} {

	vm := otto.New()
	filepath, _ := filepath2.Abs("../js/evaluate.js")

	script, err := vm.Compile(filepath, nil)

	if err != nil {
		log.Fatal(err)
	}
	_, err = vm.Run(script)

	if err != nil {
		log.Fatal(err)
	}

	if versionId == "" {
		versionId = "latest"
	}
	rsv, _ := cache.GetInstance().Get("RuleSetVersions")
	rs, _ := cache.GetInstance().Get("RuleSet")

	ruleSet := string(rs)

	ruleSetVersions := string(rsv)

	response, err := vm.Call(`evaluate`, nil, ruleSet, ruleSetVersions, ruleSetName, facts, versionId, nil, true, true)

	if err != nil {
		log.Fatal(err)
	}

	return response
}