module bitbucket.org/sahilpaudel/carbon-go-client

go 1.13

require (
	github.com/allegro/bigcache/v2 v2.2.5 // indirect
	github.com/allegro/bigcache/v3 v3.0.0
	github.com/robertkrimen/otto v0.0.0-20200922221731-ef014fd054ac
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)
